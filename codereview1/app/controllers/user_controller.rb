class UserController < ApplicationController

  def index
    @users = User.find_users(params[:q])
    @dismovie = User.select(:movie_name).uniq
  end

  def search
    @users = User.find_users(params[:q])
    @dismovie =  User.select(:movie_name).uniq
    render "index"
  end

end
