class User < ActiveRecord::Base
  class << self

    def search(query)
      self.where("movie_name LIKE ?", "%#{query}%")
    end

    def find_users(movie)
      return self.search(movie) if movie
      self.all
    end
    
  end
end
