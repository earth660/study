class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :movie_name, null:false
      t.integer :movie_eval, null: false, :limit => 5
      t.boolean :sex, deault: true
                            #sex true: man, false: womanse
      t.timestamps null: false
    end
  end
end
